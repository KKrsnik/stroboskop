# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init stroboskop
git pull https://KKrsnik@bitbucket.org/KKrsnik/stroboskop.git

```

Naloga 6.2.3:
https://bitbucket.org/KKrsnik/stroboskop/commits/eea4c57783912ed196589d7d8c211ac45d093fe9

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/KKrsnik/stroboskop/commits/238c81aff6e1406cf2e01bcb702d3ce81fa291b3

Naloga 6.3.2:
https://bitbucket.org/KKrsnik/stroboskop/commits/f51cd3806fff28d482b2263345b7a76ba8773fb6

Naloga 6.3.3:
https://bitbucket.org/KKrsnik/stroboskop/commits/63f6d345b49b455796505261d136c0026d764a5d

Naloga 6.3.4:
https://bitbucket.org/KKrsnik/stroboskop/commits/cf8866aeb3d3ce54bf6d2a14f28a364114424408

Naloga 6.3.5:

```
git checkout master
git merge izgled

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/KKrsnik/stroboskop/commits/d21835c71322c37230876b68c83344e649dfaa13

Naloga 6.4.2:
https://bitbucket.org/KKrsnik/stroboskop/commits/eb8a7a9f07f6aa7799cd86f3ee24330b6ea8c0da

Naloga 6.4.3:
https://bitbucket.org/KKrsnik/stroboskop/commits/f64dc15eed96dbf1c99e4ec50191823bff6c0141

Naloga 6.4.4:
https://bitbucket.org/KKrsnik/stroboskop/commits/40b9255887af76254b6a08c718d773ab279439c7